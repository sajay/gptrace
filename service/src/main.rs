use sqlite::State;
#[macro_use] extern crate rocket;
use rand::Rng;
use rand::distributions::Alphanumeric;
use rocket::serde::json::{Json}; //, value, json};
use std::net::SocketAddr;
use rocket::serde::{Serialize, Deserialize};

#[launch]
fn rocket() -> _ {
	rocket::build()
		.mount("/breadcrumbs/show", routes![return_data])
		.mount("/breadcrumbs/register", routes![register_device])
		.mount("/breadcrumbs/add", routes![add_coords])
}

#[post("/", data = "<data>")]
fn add_coords(data: Json<AddCoords>, remote_addr: SocketAddr) -> String {
	if !auth(&data.device, &data.password){
		return "bad login".to_string();
	}

	let connection = sqlite::open("./database.sqlite3").unwrap();

        let mut statement = connection
                .prepare("insert into coordinates (
				device,
				lat,
				lon,
				tag, 
				detail,
				ip,
				device_time
			) values (?,?,?,?,?,?,?)")
                .unwrap();

        statement.bind(1, data.device.as_str()).unwrap();
        statement.bind(2, data.lat).unwrap();
        statement.bind(3, data.lon).unwrap();
        statement.bind(4, data.tag.as_str()).unwrap();
        statement.bind(5, data.detail.as_str()).unwrap();
	statement.bind(6, format!("{:?}", remote_addr).as_str()).unwrap();
	statement.bind(7, data.device_time).unwrap();

        while let State::Row = statement.next().unwrap() {}

        return "success".to_string();
}

#[post("/", data = "<data>")]
fn register_device(data: Json<DeviceRegister>) -> String {
	let connection = sqlite::open("./database.sqlite3").unwrap();
	
	let rand_string: String = rand::thread_rng().sample_iter(&Alphanumeric).take(30).map(char::from).collect();
	
        let mut statement = connection
                .prepare("insert into devices (	password, friendly_identifier, owner_first_name, owner_last_name, details ) values (?,?,?,?,?)")
                .unwrap();

        statement.bind(1, rand_string.as_str()).unwrap();
        statement.bind(2, data.friendly_identifier.as_str()).unwrap();
	statement.bind(3, data.owner_first_name.as_str()).unwrap();
	statement.bind(4, data.owner_last_name.as_str()).unwrap();
	statement.bind(5, data.details.as_str()).unwrap();

        while let State::Row = statement.next().unwrap() {}

	return rand_string.to_string();
}

#[get("/<userid>/<pastms>")]
fn return_data(userid: String, pastms: i64) -> rocket::response::content::Html<String> {
	let connection = sqlite::open("./database.sqlite3").unwrap();

        let mut statement = connection
                .prepare("SELECT * FROM coordinates WHERE device = ? and device_time > strftime('%s','now') || substr(strftime('%f','now'),4) - ?")
                .unwrap();

        statement.bind(1, userid.as_str()).unwrap();
	statement.bind(2, pastms).unwrap();
	
	let mut response_str: String = "<!DOCTYPE html><html><table style=\"border-collapse: collapse;\" border=7>".to_string();

	for i in 0..statement.names().iter().count() {
                response_str += format!("<th>{}</th>",
			statement.names()[i]
                ).as_str();
        }


        while let State::Row = statement.next().unwrap() {
		response_str += "<tr>";
		for i in 0..statement.names().iter().count() {
			response_str += format!("<th>{}</th>", statement.read::<String>(i).unwrap_or("".to_string()) ).as_str();
		}
		response_str += "</tr>";
        }
	response_str += "</table></html>";

	return rocket::response::content::Html(response_str);
}

fn auth(userid: &String, password: &String) -> bool {
	let connection = sqlite::open("./database.sqlite3").unwrap();

        let mut statement = connection
                .prepare("SELECT password FROM devices where friendly_identifier = ?")
                .unwrap();

        statement.bind(1, userid.as_str()).unwrap();

        while let State::Row = statement.next().unwrap() {
		match statement.read::<String>(0) {
			Ok(val) => {
					if &val == password {
						return true;
					}
				}
			Err(_e) => { }
		}
        }

	return false;
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct DeviceRegister {
  friendly_identifier: String,
  owner_first_name: String,
  owner_last_name: String,
  details: String
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct AddCoords {
	device: String,
	password: String,
	lat: f64,
	lon: f64,
	tag: String,
	detail: String,
	device_time: i64
}
